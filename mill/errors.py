class LoginError(Exception):
    pass


class TokenError(Exception):
    pass


class APIError(Exception):
    pass
