from __future__ import print_function
import os
import pytest
import types
import json
from datetime import timedelta
from requests.exceptions import ConnectionError
from mill import Client
from mill.errors import LoginError


def test_invalid_url():
    with pytest.raises(ConnectionError):
        invalid_client = Client("fake", "info", "given", "http://api.fake.io")
        assert invalid_client.get_health() is False


def test_invalid_credentials():
    with pytest.raises(LoginError):
        invalid_client = Client("fake", "info", "given")
        assert invalid_client.get_health() is False


def test_get_health():
    client = Client(client_id=os.environ.get("CLIENT_ID", "client_id"),
                    client_secret=os.environ.get("CLIENT_SECRET", "client_secret"),
                    bundle="com.set.app")
    assert client.get_health() is True


def test_request_features():
    client = Client(client_id=os.environ.get("CLIENT_ID", "client_id"),
                    client_secret=os.environ.get("CLIENT_SECRET", "client_secret"),
                    bundle="com.set.app")
    response = client.request_features()
    assert response is not None
    assert isinstance(response, types.GeneratorType)


def test_request_features_lookback():
    client = Client(client_id=os.environ.get("CLIENT_ID", "client_id"),
                    client_secret=os.environ.get("CLIENT_SECRET", "client_secret"),
                    bundle="com.set.app")
    response = client.request_features(lookback=timedelta(days=2), types=["events"])
    assert response is not None
    assert isinstance(response, types.GeneratorType)


def test_download_features(tmpdir):
    client = Client(client_id=os.environ.get("CLIENT_ID", "client_id"),
                    client_secret=os.environ.get("CLIENT_SECRET", "client_secret"),
                    bundle="com.set.app")
    with open(str(tmpdir.join("features.ndjson")), "w+") as f:
        client.download_features(f, lookback=timedelta(days=0.5), types=["events"])
        f.seek(0)
        response = json.loads("[" + ",".join(f.readlines()) + "]")
    assert response is not None
    assert len(response)
